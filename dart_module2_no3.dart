class App {
  String appname;
  String category;
  String developer;
  int year;

  App(this.appname, this.category, this.developer, this.year);

  void describe() {
    print(
        "App name: $appname\nCategory: $category\ndveloper:$developer\nYear:$year");
  }

  void toCapital() {
    print(appname.toUpperCase());
  }
}

void main() {
  App myapp = App("wumdrop", "fleet management", "Simon Hartley", 2015);

  myapp.describe();
  myapp.toCapital();
}
